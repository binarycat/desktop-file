//! various constants defined by the standard, to avoid typo errors.
//! these constants mostly follow the original CamelCase instead of the conventional rust UPPER_CASE

#![allow(non_upper_case_globals)]

macro_rules! def_str_consts {
	() => ();
	($x:ident) => (pub const $x: &str = stringify!($x););
	($x:ident,) => (def_str_consts{$x});
	($x:ident, $($xs:ident),+) =>
		(def_str_consts!{$x}
		 def_str_consts!{$($xs),+});
}

pub mod key {
	//! constants for keys that are defined in the spec.
	//! it is recommended to use the get_* or set_* family of methods instead, if possible.
	def_str_consts!{
		Type,
		Version,
		Name,
		GenericName,
		NoDisplay,
		Comment,
		Icon,
		Hidden,
		OnlyShowIn,
		NotShowIn,
		DBusActivatable,
		TryExec,
		Exec,
		Path,
		Terminal,
		Actions,
		MimeType,
		Categories,
		Implements,
		Keywords,
		StartupNotify,
		StartupWMClass,
		URL,
		PrefersNonDefaultGPU,
		SingleMainWindow
	}
}

pub mod entry_type {
	def_str_consts!{Application, Link, Directory}
}

/// version of the desktop entry spec this library is built for.
/// for the Version field.
pub const VERSION: &str = "1.5";
