/// represents a possible value for LC_MESSAGES
pub struct Locale {
	pub lang: String,
	pub country: Option<String>,
	pub encoding: Option<String>,
	pub modifier: Option<String>,
}

impl Locale {
	pub fn parse(src: &str) -> Self {
		let mut r = Locale{
			lang: "".to_string(),
			country: None,
			encoding: None,
			modifier: None,
		};
		let mut s = src;
		if let Some((rest, m)) = s.split_once('@') {
			r.modifier = Some(m.to_string());
			s = rest;
		}
		if let Some((rest, e)) = s.split_once('.') {
			r.encoding = Some(e.to_string());
			s = rest;
		}
		if let Some((rest, c)) = s.split_once('_') {
			r.country = Some(c.to_string());
			s = rest;
		}
		r.lang = s.to_string();
		return r;
	}
	/// parse locale from current value of LC_MESSAGES
	pub fn env() -> Option<Self> {
		if let Ok(loc) = std::env::var("LC_MESSAGES") {
			if loc != "" {
				return Some(Self::parse(&loc));
			}
		}
		return None;
	}
}
