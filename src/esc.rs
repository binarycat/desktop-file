//! escape various datatypes
use std::borrow::Borrow;

#[derive(thiserror::Error, Debug)]
pub enum EscapeError {
	/// control charachter that cannot be escaped, like DEL
	#[error("cannot escape '{0:?}'")]
	CannotEscape(char),
}

pub type Result<T> = std::result::Result<T, EscapeError>;

// TODO: there needs to be a seperate but similar function for quoting exec args
// also, similar function for use in escape_strings?
fn escape_char(c: char) -> Result<Option<char>> {
	return Ok(Some(match c {
		' ' => 's',
		'\n' => 'n',
		'\t' => 't',
		'\r' => 'r',
		'\\' => '\\',
		_ => {
			if c.is_ascii_control() {
				return Err(EscapeError::CannotEscape(c));
			} else {
				return Ok(None);
			}
		}
	}));
}

pub fn escape_string(raw: &str) -> Result<String> {
	let mut s = String::with_capacity(raw.len());
	for c in raw.chars() {
		if let Some(ec) = escape_char(c)? {
			s.push('\\');
			s.push(ec);
		} else {
			s.push(c);
		}
	}
	return Ok(s);
}

pub fn escape_strings<'a, S: Borrow<str> + 'a, V: Iterator<Item = &'a S>>(iter: V)
													 -> Result<String> {
	let mut s = String::new();
	for raw in iter {
		let raw_str: &str = &raw.borrow();
		for c in raw_str.chars() {
			if c == ';' {
				s.push('\\');
				s.push(';');
			} else if let Some(ec) = escape_char(c)? {
				s.push('\\');
				s.push(ec);
			} else {
				s.push(c);
			}
		}
		s.push(';');
	}
	return Ok(s);
}

#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	fn esc() {
		assert_eq!(escape_strings(["foo;bar", "baz", ""][..].iter()).unwrap(),
				   r"foo\;bar;baz;;");
	}
}
