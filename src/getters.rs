#![allow(non_snake_case)]

use crate::{
	consts::key,
	group::MAIN,
	unesc,
	unesc::{
		unescape_strings,
		unescape_string,
		unescape_boolean,
	},
	Document,
	locale::Locale,
};

#[derive(thiserror::Error, Debug)]
pub enum GetError {
	#[error("failed to unescape field: {0}")]
	UnescapeError(unesc::Error),
	#[error("missing a required key")]
	MissingRequiredKey,
}

impl From<unesc::Error> for Error {
	fn from(e: unesc::Error) -> Error {
		Error::UnescapeError(e)
	}
}

// TODO: at some point add #[deprecated] to these and rewrite stuff to remove them
// having a bunch of types named just Error is confusing, expecially when they appear on methods
pub(crate) type Error = GetError;

pub type Result<T> = std::result::Result<T, Error>;

fn required<T>(x: Result<Option<T>>) -> Result<T> {
	if let Some(y) = x? {
		Ok(y)
	} else {
		Err(Error::MissingRequiredKey)
	}
}

// TODO: autogenerate doc comments
macro_rules! getter {
	($func:ident bool $key:ident) =>
		(pub fn $func(&self) -> Result<Option<bool>> {
			self.get_boolean(key::$key)
		});
	($func:ident str $key:ident) =>
		(pub fn $func(&self) -> Result<Option<String>> {
			self.get_string(key::$key)
		});
	($func:ident [str] $key:ident) =>
		(pub fn $func(&self) -> Result<Vec<String>> {
			self.get_strings(key::$key)
		});
	($func:ident lstr $key:ident) =>
		(pub fn $func(&self, loc: Option<&Locale>) -> Result<Option<String>> {
			self.get_localestring(key::$key, loc)
		});
	($func:ident [lstr] $key:ident) =>
		(pub fn $func(&self, loc: Option<&Locale>) -> Result<Vec<String>> {
			self.get_localestrings(key::$key, loc)
		});
}

impl<'a> Document<'a> {
	/// get and unescape a field with the `string(s)` type.
	/// searches the `Desktop Entry` group.
	/// if the field does not exist, return an empty Vec.
	pub fn get_strings(&self, key: &str) -> Result<Vec<String>> {
		// TODO: we end up double-allocating here, we could eliminate this by
		// having a version of lookup() that somehow returns the underlying Cow
		if let Some(raw) = self.lookup(MAIN, key) {
			return Ok(unescape_strings(&raw)?);
		} else {
			return Ok(vec![]);
		}
	}

	/// get and unescape a field with the `string` type.
	/// searches the `Desktop Entry` group.
	pub fn get_string(&self, key: &str) -> Result<Option<String>> {
		if let Some(raw) = self.lookup(MAIN, key) {
			return Ok(Some(unescape_string(&raw)?));
		} else {
			return Ok(None);
		}
	}

	pub fn get_localestring(&self, key: &str, loc: Option<&Locale>)
							-> Result<Option<String>>{
		if let Some(loc) = loc {
			if let Some(raw) = self.lookup_localized(MAIN, key, loc) {
				return Ok(Some(unescape_string(&raw)?));
			} else {
				return Ok(None);
			}
		} else {
			return self.get_string(key);
		}
	}

	pub fn get_localestrings(&self, key: &str, loc: Option<&Locale>)
							-> Result<Vec<String>>{
		if let Some(loc) = loc {
			if let Some(raw) = self.lookup_localized(MAIN, key, loc) {
				return Ok(unescape_strings(&raw)?);
			} else {
				return Ok(vec![]);
			}
		} else {
			return self.get_strings(key);
		}
	}

	pub fn get_boolean(&self, key: &str) -> Result<Option<bool>> {
		if let Some(raw) = self.lookup(MAIN, key) {
			return Ok(Some(unescape_boolean(&raw)?));
		} else {
			return Ok(None);
		}
	}

	// end of generic getters, beginning of specific getters

	/// returns Error::MissingRequiredKey if the Type key is not found.
	pub fn get_Type(&self) -> Result<String> {
		required(self.get_string(key::Type))
	}

	getter!(get_Version str Version);

	/// get and unescape the Name field for the given locale.
	/// if loc is None, get the unlocalized value.
	pub fn get_Name(&self, loc: Option<&Locale>) -> Result<String> {
		// don't use macro for required fields
		required(self.get_localestring(key::Name, loc))
	}

	getter!(get_GenericName lstr GenericName);
	getter!(get_NoDisplay bool NoDisplay);
	getter!(get_Comment lstr Comment);
	getter!(get_Icon lstr Icon);
	getter!(get_Hidden bool Hidden);
	getter!(get_OnlyShowIn [str] OnlyShowIn);
	getter!(get_NotShowIn [str] NotShowIn);
	getter!(get_DBusActivatable bool DBusActivatable);
	getter!(get_TryExec str TryExec);
	getter!(get_Path str Path);
	getter!(get_Terminal bool Terminal);
	///// get and unescape the Actions field.
	///// if the Actions field does not exist, returns an empty Vec.
	getter!(get_Actions [str] Actions);
	getter!(get_MimeType [str] MimeType);
	getter!(get_Categories [str] Categories);
	getter!(get_Implements [str] Implements);
	getter!(get_Keywords [lstr] Keywords);
	getter!(get_StartupNotify bool StartupNotify);
	getter!(get_StartupWMClass str StartupWMClass);
	/// get the URL of a Link type entry.
	pub fn get_URL(&self) -> Result<String> {
		required(self.get_string(key::URL))
	}
	getter!(get_PrefersNonDefaultGPU bool PrefersNonDefaultGPU);
	getter!(get_SingleMainWindow bool SingleMainWindow);
}
