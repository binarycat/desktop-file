//! library for manipulating .desktop files
//!
//! based of off <https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html>
//!
//! limitations: does not implement depricated features

// TODO: make error naming convention uniform

pub mod group;
pub mod parse;
pub mod locale;
pub mod consts;
pub mod esc;
pub mod unesc;
//pub mod exec;
//pub mod action;
mod getters;
mod setters;


use crate::locale::Locale;

use std::{
	borrow::Cow,
	fmt,
};

pub use consts::key;
pub use getters::GetError;
pub use setters::SetError;

#[derive(PartialEq, Clone, Copy)]
pub(crate) enum LineType {
	// full comment line including #
	// also represents empty lines
	Comment,
	Group,
	Pair,
}

// we use C-style enum+struct instead of a rust enum with fields so that we can
// more easily abstract away the commonalities of each line type.
pub(crate) struct Line<'a> {
	ty: LineType,
	a: Cow<'a, str>,
	// only used for Pair
	b: Cow<'a, str>,
}

impl<'a> Line<'a> {
	fn mk(ty: LineType, a: &'a str, b: &'a str) -> Self {
		Line{
			ty,
			a: Cow::Borrowed(a),
			b: Cow::Borrowed(b),
		}
	}
}

/// represents a single desktop entry.
///
/// the main datatype of the libary.
pub struct Document<'a> {
	lines: Vec<Line<'a>>,
	// empty string = unknown
	filename: Cow<'a, str>,
}

impl<'a> fmt::Display for Document<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let mut firstline = true;
		for line in &self.lines {
			if !firstline {
				write!(f, "\n")?;
			}
			firstline = false;
			match line.ty {
				LineType::Comment => write!(f, "{}", &line.a)?,
				LineType::Group => write!(f, "[{}]", &line.a)?,
				LineType::Pair => write!(f, "{}={}", &line.a, &line.b)?,
			}
		}
		return Ok(());
	}
}

impl<'a> Document<'a> {
	fn empty() -> Self {
		return Document{
			lines: Vec::new(),
			filename: "".into(),
		};
	}
	
	pub fn new_app(name: &str, exec: &str)
				   -> Result<Self, SetError> {
		let mut doc = Self::empty();
		doc.set_Version("1.5")?;
		doc.set_Type(consts::entry_type::Application)?;
		doc.set_Name(name)?;
		doc.set_Exec(exec)?;
		return Ok(doc);
	}

	pub fn new_link(name: &str, url: &str)
					-> Result<Self, SetError> {
		let mut doc = Self::empty();
		doc.set_Version("1.5")?;
		doc.set_Name(name)?;
		doc.set_URL(url)?;
		return Ok(doc);
	}

	// TODO: new_dir(), not implemented because info on the Directory type is sparse

	fn lookup_index(&self, groupname: &str, key: &str) -> Option<usize> {
		let mut in_group = false;
		for (i, line) in self.lines.iter().enumerate() {
			match line.ty {
				LineType::Group => in_group = line.a == groupname,
				LineType::Pair => if in_group && key == line.a {
					return Some(i);
				},
				_ => {},
			}
		}
		return None;
	}

	/// find the raw string value associated with the given key in a given group
	/// NOTE: does not perform unescaping
	pub fn lookup(&self, groupname: &str, key: &str) -> Option<String> {
		if let Some(i) = self.lookup_index(groupname, key) {
			Some(self.lines[i].b.clone().into_owned())
		} else {
			None
		}
	}

	/// perform the operation described at <https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#localized-keys>
	pub fn lookup_localized(&self, groupname: &str, key: &str, loc: &Locale)
						 -> Option<String> {
		if loc.country.is_some() && loc.modifier.is_some() {
			if let Some(r) = self.lookup(
				groupname,
				&format!("{}[{}_{}@{}]",
						key, loc.lang,
						loc.country.as_ref().unwrap(), loc.modifier.as_ref().unwrap())) {
				return Some(r);
			}
		}
		if let Some(country) = loc.country.as_ref() {
			if let Some(r) = self.lookup(
				groupname,
				&format!("{}[{}_{}]",
						key, loc.lang, country)) {
				return Some(r);
			}
		}
		if let Some(modifier) = loc.modifier.as_ref() {
			if let Some(r) = self.lookup(
				groupname,
				&format!("{}[{}@{}]",
						key, loc.lang, modifier)) {
				return Some(r);
			}
		}
		if let Some(r) = self.lookup(
			groupname,
			&format!("{}[{}]", key, loc.lang)) {
			return Some(r);
		}
		return self.lookup(groupname, key);
	}

	/// delete a key-value pair from the specified group
	/// returns false if the group does not contain `key`
	pub fn delete(&mut self, groupname: &str, key: &str) -> bool {
		if let Some(i) = self.lookup_index(groupname, key) {
			self.lines.remove(i);
			return true;
		}
		return false;
	}

	/// calls lookup_localized if a locale is given, otherwise defaults to
	/// regular lookup
	pub fn lookup_maybe_localized(
		&self, groupname: &str, key: &str, loc: Option<&Locale>)
		-> Option<String> {
		if let Some(loc) = loc {
			self.lookup_localized(groupname, key, loc)
		} else {
			self.lookup(groupname, key)
		}
	}

	

	/// WARNING: if there are localized keys, set() will not affect them, and so the new value may be overridden by existing keys.
	// rational for type signature: groupname key and val with almost always be either a static str or a heap string, but may not all be the same type
	pub fn set<G: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>,
			   K: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>,
			   V: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>>(
		&mut self, groupname: G, key: K, val: V) {
		// we add a bit of complexity by tracking this,
		// but in exchange we reduce the likelyhood of accidentally decoupling a comment from the group it comments
		let mut last_non_comment = 0;
		let mut in_group = false;
		for (i, line) in self.lines.iter().enumerate() {
			match line.ty {
				LineType::Group => {
					let was_in_group = in_group;
					in_group = groupname == line.a;
					if was_in_group && !in_group {
						// case B: group already exists, but key doesn't
						self.lines.insert(last_non_comment+1, Line{
							ty: LineType::Pair,
							a: key.into(),
							b: val.into(),
						});
						return;
					}
				},
				LineType::Pair => if in_group && key == line.a {
					// case A: key already exists in given group, mutate
					self.lines[i].b = val.into();
					return;
				},
				_ => {},
			}
			if line.ty != LineType::Comment {
				last_non_comment = i;
			}
		}
		// case C: group doesn't exist
		self.lines.push(Line{
			ty: LineType::Group,
			a: groupname.into(),
			b: "".into(),
		});
		self.lines.push(Line{
			ty: LineType::Pair,
			a: key.into(),
			b: val.into(),
		})
	}

	/// move all of a Document's string data to the heap.
	///
	/// useful for when you want to return a Document whose lifetime is about to expire.
	pub fn into_owned(self) -> Document<'static> {
		Document{
			filename: Cow::Owned(self.filename.into_owned()),
			lines: self.lines.into_iter().map(
				|ln| Line{
					ty: ln.ty,
					a: Cow::Owned(ln.a.into_owned()),
					b: Cow::Owned(ln.b.into_owned()),
				}).collect(),
		}
	}
	// TODO: exec()
	// TODO?: delocalize() method that deletes all localized values for a given key
}

#[cfg(test)]
mod tests {
    use super::*;

	const EXAMPLE1: &str = r#"
# this is a test desktop entry
[Desktop Entry]
Version=1.0
Type=Application
Name=Foo Viewer
Name[tok]=ilo oko Foo
Name[tok@pu]=ilo lukin Foo
Comment=The best viewer for Foo objects available!
TryExec=fooview
Exec=fooview %F
Icon=fooview
MimeType=image/x-foo;
Actions=Gallery;Create;

[Desktop Action Gallery]
Exec=fooview --gallery
Name=Browse Gallery

[Desktop Action Create]
Exec=fooview --create-new
Name=Create a new Foo!
Icon=fooview-new
"#;
	
    #[test]
    fn it_works() {
        let mut doc1 = Document::parse(EXAMPLE1).unwrap();
        assert_eq!(doc1.lookup(group::MAIN, key::Type).unwrap(), "Application");
		assert_eq!(doc1.lookup("Desktop Action Gallery", key::Exec).unwrap(),
				   "fooview --gallery");
		let tok = Locale::parse("tok_US");
		assert_eq!(doc1.lookup_localized(group::MAIN, key::Name, &tok).unwrap(), "ilo oko Foo");
		doc1.set(group::MAIN, key::NoDisplay, "true");
		assert_eq!(doc1.lookup(group::MAIN, key::NoDisplay).unwrap(), "true");
		println!("{}", doc1);
		assert_eq!(doc1.to_string(), r#"
# this is a test desktop entry
[Desktop Entry]
Version=1.0
Type=Application
Name=Foo Viewer
Name[tok]=ilo oko Foo
Name[tok@pu]=ilo lukin Foo
Comment=The best viewer for Foo objects available!
TryExec=fooview
Exec=fooview %F
Icon=fooview
MimeType=image/x-foo;
Actions=Gallery;Create;
NoDisplay=true

[Desktop Action Gallery]
Exec=fooview --gallery
Name=Browse Gallery

[Desktop Action Create]
Exec=fooview --create-new
Name=Create a new Foo!
Icon=fooview-new
"#);
		assert_eq!(doc1.get_Actions().unwrap()[1], "Create");
		assert_eq!(doc1.get_Actions().unwrap().len(), 2);
		assert_eq!(doc1.get_Name(None).unwrap(), "Foo Viewer");
		assert_eq!(doc1.get_Name(Some(&tok)).unwrap(), "ilo oko Foo");
		/*let act1 = doc1.get_action_by_id("Create", None).unwrap();
		assert_eq!(act1.id, "Create");
		assert_eq!(act1.name, "Create a new Foo!");
		assert_eq!(act1.icon.unwrap(), "fooview-new");
		assert_eq!(act1.exec.unwrap(), "fooview --create-new");*/
    }
}
