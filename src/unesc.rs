//! unescape various datatypes

#[derive(thiserror::Error, Debug)]
pub enum UnescapeError {
	/// string literal ends in unescaped backslash
	#[error("string literal ends in unescaped backslash")]
	TrailingBackslash,
	/// value other that true/false/1/0 for boolean
	#[error("invalid boolean literal: {0}")]
	InvalidBoolean(String),
}

// depricated alias
pub(crate) type Error = UnescapeError;

pub type Result<T> = std::result::Result<T, Error>;

/// unescape a backslash escape sequence
fn unescape_char(c: char) -> char {
	match c {
		's' => ' ',
		'n' => '\n',
		't' => '\t',
		'r' => '\r',
		_ => c,
	}
}

pub fn unescape_strings(s: &str) -> Result<Vec<String>> {
	if s.len() == 0 { return Ok(vec![]) }
	let mut v = Vec::new();
	let mut iter = s.chars();
	v.push(String::new());
	loop {
		let Some(c) = iter.next() else {
			// ignore up to one trailing semicolon
			if v.last().unwrap() == "" {
				v.pop();
			}
			return Ok(v);
		};
		if c == '\\' {
			let Some(ec) = iter.next() else {
				return Err(Error::TrailingBackslash);
			};
			v.last_mut().unwrap().push(unescape_char(ec));
		} else if c == ';' {
			v.push(String::new());
		} else {
			v.last_mut().unwrap().push(c);
		}
	}
}

pub fn unescape_string(raw: &str) -> Result<String> {
	let mut s = String::new();
	let mut iter = raw.chars();
	loop {
		let Some(c) = iter.next() else {
			return Ok(s)
		};
		if c == '\\' {
			let Some(ec) = iter.next() else {
				return Err(Error::TrailingBackslash);
			};
			s.push(unescape_char(ec));
		} else {
			s.push(c);
		}
	}
}

/// parses a boolean.
///
/// for compatibility with pre-1.0 desktop files,
/// 1/0 are treated as aliases for true/false
pub fn unescape_boolean(raw: &str) -> Result<bool> {
	match raw {
		"true" | "1" => Ok(true),
		"false" | "0" => Ok(false),
		_ => Err(Error::InvalidBoolean(raw.to_string())),
	}
}
