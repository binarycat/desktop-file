#![allow(non_snake_case)]

use crate::{
	Document,
	esc::{
		EscapeError,
		escape_string,
		escape_strings,
	},
	group::MAIN,
	key,
};
use std::borrow::{Cow, Borrow};

#[derive(thiserror::Error, Debug)]
pub enum SetError {
	#[error("failed to escape value: {0}")]
	EscapeError(#[from] EscapeError),
	#[error("failed to get key: {0}")]
	GetError(#[from] crate::GetError)
}

pub type Result<T> = std::result::Result<T, SetError>;

// TODO: auto doc comments
macro_rules! setter {
	($func:ident bool $key:ident) =>
		(pub fn $func(&mut self, val: bool) {
			self.set_boolean(key::$key, val)
		});
	($func:ident str $key:ident) =>
		(pub fn $func(&mut self, val: &str) -> Result<()> {
			self.set_string(key::$key, val)
		});
	($func:ident $add_func:ident [str] $key:ident) =>
		(pub fn $func<S: ToString>(&mut self, vals: &[String]) -> Result<()> {
			self.set_strings(key::$key, vals)
		}
		 pub fn $add_func<S: ToString>(&mut self, vals: &[S]) -> Result<()> {
			 self.add_strings(key::$key, vals)
		 });
}

impl<'a> Document<'a> {
	/// escape and set a string-valued key in the `Desktop Entry` group.
	pub fn set_string<S: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>>(
		&mut self, key: S, val: &str) -> Result<()> {
		self.set(MAIN, key, escape_string(val)?);
		return Ok(());
	}

	pub fn set_strings<K: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>>(
		&mut self, key: K, vals: &[String]) -> Result<()> {
		self.set(MAIN, key, escape_strings(vals.iter())?);
		return Ok(());
	}

	/// append strings to the list of strings in a string(s)-valued key
	pub fn add_strings<S: ToString,
					   K: Into<Cow<'a, str>> + Borrow<str> + PartialEq<Cow<'a, str>>,
					   >(&mut self, key: K, vals: &[S])
						 -> Result<()> {
		let mut old = self.get_strings(key.borrow())?;
		for v in vals {
			old.push(v.to_string());
		}
		self.set_strings(key, &old)
	}

	pub fn set_boolean<K: Into<Cow<'a, str>> + PartialEq<Cow<'a, str>>>(
		&mut self, key: K, val: bool) {
		self.set(MAIN, key, if val { "true" } else { "false" });
	}

	// TODO: localization support?
	
	setter!(set_Type str Type);
	setter!(set_Version str Version);
	setter!(set_Name str Name);
	setter!(set_GenericName str GenericName);
	setter!(set_NoDisplay str NoDisplay);
	setter!(set_Comment str Comment);
	setter!(set_Icon str Icon);
	setter!(set_Hidden bool Hidden);
	setter!(set_OnlyShowIn add_OnlyShowIn [str] OnlyShowIn);
	setter!(set_NotShowIn add_NotShowIn [str] NotShowIn);
	setter!(set_DBusActivatable bool DBusActivatable);
	setter!(set_TryExec str TryExec);
	setter!(set_Exec str Exec);
	setter!(set_Path str Path);
	setter!(set_Terminal bool Terminal);
	setter!(set_Actions add_Actions [str] Actions);
	setter!(set_MimeType add_MimeType [str] MimeType);
	setter!(set_Categories add_Categories [str] Categories);
	setter!(set_Implements add_Implements [str] Implements);
	setter!(set_Keywords add_Keywords [str] Keywords);
	setter!(set_StartupNotify bool StartupNotify);
	setter!(set_StartupWMClass str StartupWMClass);
	setter!(set_URL str URL);
	setter!(set_PrefersNonDefaultGPU bool PrefersNonDefaultGPU);
	setter!(set_SingleMainWindow bool SingleMainWindow);
}
