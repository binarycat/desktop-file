use crate::{
	Document,
	locale::Locale,
	GetError,
};

use std::{
	process::Command,
	ffi::OsStr,
};

#[derive(Debug)]
#[must_use]
pub enum ExecError {
	UnterminatedQuote,
	/// invalid field code
	InvalidCode(char),
	/// command line is completly empty
	NoCommand,
	/// doublequote in unexpected places.
	UnexpectedQuote,
	/// the program has a %f or %F field code, and has been passed a url with a
	/// scheme other than `file:///`
	/// may be handled by downloading remote resources into a temporary file,
	/// then passing that file in place of the original url.
	ProgramRequiresFile,
	/// the program has a %f or %u field code, and has been passed multiple urls.
	ProgramCannotHandleMany,
	/// encountered an error when looking up a key.
	GetError(GetError),
}

impl From<GetError> for ExecError {
	fn from(e: GetError) -> Self { Self::GetError(e) }
}

pub type Result<T> = std::result::Result<T, ExecError>;

/// fully parsed value for the Exec key
pub struct Exec {
	cmd: String,
	args: Vec<String>,
}

#[derive(PartialEq)]
enum ParseMode {
	// mode before the start of a new argument
	Space,
	// part of a unquoted argument
	Plain,
	// part of a quoted argument
	Quote,
	// charachter after a backslash
	Slash,	
}

impl Exec {
	/// parses the value of an Exec key.
	///
	/// NOTE: the argument should have a single level of unescaping done already.
	/// `get_Exec()` will handle this unescaping.
	pub fn parse(quoted: &str) -> Result<Self> {
		//if quoted == "" { 
		use ParseMode::*;
		let mut m = Space;
		let mut v: Vec<String> = Vec::new(); //vec![String::new()];
		for c in quoted.chars() {
			if m == Slash {
				v.last_mut().unwrap().push(c)
			} else if c == '"' {
				if m == Space {
					v.push(String::new());
					m = Quote;
				} else if m == Quote {
					m = Space;
				} else {
					return Err(ExecError::UnexpectedQuote);
				}
			} else if c == '\\' {
				m = Slash;
			} else if c == ' ' && m == Plain {
				m = Space;
			} else {
				if m == Space {
					v.push(String::new());
					m = Plain;
				}
				v.last_mut().unwrap().push(c);
			}
		}
		if v.len() == 0 {
			return Err(ExecError::NoCommand);
		}
		let cmd = v.remove(0);
		return Ok(Exec{ cmd, args: v });
	}

	pub fn interpolate<'a, S: AsRef<str> + AsRef<OsStr>>(
		&self,
		urls: &[S],
		doc: Option<&Document<'a>>,
		loc: Option<&Locale>,
	) -> Result<Command> {
		let mut cmd = Command::new(self.cmd.clone());
		for arg in &self.args {
			if arg == "%U" {
				for url in urls {
					cmd.arg(url);
				}
			} else if arg == "%F" {
				for url in urls {
					cmd.arg(to_file(url.as_ref())?);
				}
			} else if arg == "%i" {
				if let Some(d) = doc {
					if let Some(icon) = d.get_Icon(loc)? {
						cmd.arg("--icon").arg(&icon);
					}
				}
			} else {
				let mut narg = arg.clone();
				if arg.contains("%f") {
					if replace_single(&mut narg, "%f", urls)? {
						continue;
					}
				} else if arg.contains("%u") {
					if replace_single(&mut narg, "%u", urls)? {
						continue;
					}
				}
				if let Some(d) = doc {
					narg = narg.replace("%c", &d.get_Name(loc)?);
					narg = narg.replace("%k", &d.filename);
				}
				narg = narg.replace("%%", "%");
				cmd.arg(narg);
			}
		}
		if let Some(d) = doc {
			if let Some(p) = d.get_Path()? {
				cmd.current_dir(p);
			}
		}
		return Ok(cmd);
	}

	// TODO: format Exec to string.  also everything in here needs to be heavily tested, otherwise there's a chance for vulnerabilities. 
}

fn to_file<'a>(url: &'a str) -> Result<&'a str> {
	if url.starts_with("file:///") {
		Ok(&url[7..])	
	} else {
		Err(ExecError::ProgramRequiresFile)
	}
}

// returns true if the argument should be skipped
fn replace_single<S: AsRef<str>>(arg: &mut String, pat: &str, urls: &[S])
								 -> Result<bool> {
	if urls.is_empty() {
		return Ok(true);
	}
	if urls.len() > 1 {
		return Err(ExecError::ProgramCannotHandleMany);
	}
	let mut u = urls[0].as_ref();
	if pat == "%f" {
		u = to_file(u)?;
	}
	*arg = arg.replace(pat, u);
	return Ok(false);
}
		
#[cfg(test)]
mod tests {
	use super::*;

	
	#[test]
	fn parse() {
		let e1 = Exec::parse("foo --bar \"foo bar\"").unwrap();
		assert_eq!(e1.cmd, "foo");
		assert_eq!(e1.args.len(), 2);
		assert_eq!(e1.args[0], "--bar");
		assert_eq!(e1.args[1], "foo bar");
	}

	#[test]
	fn interpolate() {
		let e1 = Exec::parse("/bin/openfoo --files %F").unwrap();
		let c1 = e1.interpolate(
			&["file:///tmp/a.foo", "file:///usr/share/b.foo"],
			None, None).unwrap();
		let args1: Vec<&OsStr> = c1.get_args().collect();
		assert_eq!(args1[0], "--files");
		assert_eq!(args1[1], "/tmp/a.foo");
		assert_eq!(args1[2], "/usr/share/b.foo");
		assert_eq!(args1.len(), 3);
	}
}
