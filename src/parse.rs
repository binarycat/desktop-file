use crate::{Line, LineType, Document};
use std::borrow::Cow;

#[derive(thiserror::Error, Debug)]
pub enum ParseError {
	#[error("expected '=' to delimit key-value pair")]
	ExpectedEq,
	#[error("expected '[' at end of group")]
	ExpectedGroupEnd,
}

pub(crate) type Error = ParseError;

type Result<T> = std::result::Result<T, Error>;

fn parse_lines<'a, T: Iterator<Item = &'a str>>(lines: T) -> Result<Document<'a>> {
	let mut stmts: Vec<Line> = Vec::new();
	for ln in lines {
		use LineType::*;
		stmts.push(match ln.chars().nth(0) {
			//None => Line::mk(Comment, "", ""),
			None | Some('#') => Line::mk(Comment, ln, ""),
			Some('[') => if ln.chars().nth_back(0) != Some(']') {
				return Err(Error::ExpectedGroupEnd);
			} else {
				Line::mk(Group, &ln[1..ln.len()-1], "")
			},
			_ => if let Some((k, v)) = ln.split_once('=') {
				Line::mk(Pair, k, v)
			} else {
				return Err(Error::ExpectedEq);
			},
		});
	}
	return Ok(Document{ lines: stmts, filename: Cow::Borrowed(""), });
}

impl<'a> Document<'a> {
	pub fn parse(src: &'a str) -> Result<Self> {
		return parse_lines(src.split('\n'));
	}
}
