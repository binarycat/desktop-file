use crate::{
	Document,
	key,
	locale::Locale,
	unesc::unescape_string,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ActionError {
	#[error("no action with id {0}")]
	NoActionWithId(String),
	#[error("error unescaping value: {0}")]
	UnescError(#[from] crate::unesc::Error),
}

/*impl From<GetError> for Error {
	fn from(e: GetError) -> Self {
		Self::GetError(e)
	}
}*/

pub type Result<T> = std::result::Result<T, ActionError>;

// mark non exhastive, we may want to add a field for localized fields in 
#[non_exhaustive]
pub struct Action {
	pub id: String,
	pub name: String,
	pub icon: Option<String>,
	pub exec: Option<String>,
}

impl Action {
	pub fn new(id: String, name: String) -> Action {
		Action{ id, name, icon: None, exec: None }
	}
}

impl<'a> Document<'a> {
	pub fn get_action_by_id(&self, id: &str, loc: Option<&Locale>)
							-> Result<Action> {
		let group = format!("Desktop Action {}", id);
		// this once again would be more effecient if lookup could return Cow directly

		let Some(raw_name) = self.lookup_maybe_localized(&group, key::Name, loc) else {
			return Err(ActionError::NoActionWithId(id.to_string()));
		};
		let raw_icon = self.lookup_maybe_localized(&group, key::Icon, loc);
		let raw_exec = self.lookup(&group, key::Exec);
		let mut act =
			Action::new(id.to_string(), unescape_string(&raw_name)?);
		if let Some(raw_icon) = raw_icon {
			act.icon = Some(unescape_string(&raw_icon)?);
		}
		if let Some(raw_exec) = raw_exec {
			act.exec = Some(unescape_string(&raw_exec)?);
		}
		return Ok(act);
	}
}
